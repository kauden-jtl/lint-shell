FROM alpine:3.13

RUN apk add --no-cache shellcheck 
 
CMD ["/usr/bin/shellcheck"]
